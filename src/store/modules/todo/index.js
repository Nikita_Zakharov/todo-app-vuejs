import state from "@/store/modules/todo/state";
import mutations from "@/store/modules/todo/mutations";
import getters from "@/store/modules/todo/getters";
import actions from "@/store/modules/todo/actions";

export default {state, mutations, getters, actions}
