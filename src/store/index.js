import Vue from 'vue';
import Vuex from 'vuex';
import todo from '@/store/modules/todo/index'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
      todo: {
          namespaced: true,
          ...todo
      }
    },
})

export default store
