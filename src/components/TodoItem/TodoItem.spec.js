import {shallowMount} from "@vue/test-utils";
import TodoItem from "@/components/TodoItem/TodoItem";
import {stubsElementUI} from "../../../tests/utils";


const todo = { id: 1, name: 'read a book', done: false }

describe('unit tests for TodoItem component', () => {
    it('should get correct props', () => {
        const wrapper = shallowMount(TodoItem, {
            stubs: stubsElementUI,
            propsData: {
                todo,
                done: false
            }
        })
        expect(wrapper.props('todo')).toBe(todo)
    })
})
