import {mount} from "@vue/test-utils";
import TodoList from "@/components/TodoList/TodoList";
import TodoItem from "@/components/TodoItem/TodoItem";
import {stubsElementUI} from "../../../tests/utils";

const todoList = [
    {id: 1, name: 'buy bread', done: false},
    {id: 2, name: 'gym', done: false}
]

describe('unit tests for TodoList component', () => {
    it('should render items by prop items default value', () => {
        const wrapper = mount(TodoList, {
            stubs: stubsElementUI,
            propsData: {
                items: undefined
            }
        });
        expect(wrapper.props('items')).toStrictEqual([])
        expect(wrapper.findAllComponents(TodoItem).length).toBe(0)
    })
    it.todo('should render all TodoItem components')
})
