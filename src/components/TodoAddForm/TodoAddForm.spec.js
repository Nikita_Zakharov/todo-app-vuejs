import {shallowMount, createLocalVue} from "@vue/test-utils";
import TodoAddForm from "@/components/TodoAddForm/TodoAddForm";
import todoModule from '@/store/modules/todo/index'
import Vuex from 'vuex'
import {stubsElementUI} from "../../../tests/utils";

const localVue = createLocalVue()
localVue.use(Vuex)

describe('unit tests for TodoAddForm component', () => {
    let store;

    beforeEach(() => {
        store = new Vuex.Store({
            modules: {
                todo: {namespaced: true, ...todoModule}
            }
        })
    })
    it.todo('should add new todo to store')
    it('should clear form field name on submit', () => {
        const wrapper = shallowMount(TodoAddForm, {stubs: stubsElementUI, store, localVue});
        wrapper.setData({todoName: 'buy bread',})
        wrapper.vm.handleSubmit();
        expect(wrapper.vm.todoName).toBe('')
    })
})
